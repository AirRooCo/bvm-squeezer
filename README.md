# BVM squeezer

[Operating Vedio on Facebook](https://www.facebook.com/groups/fablabtaipei/permalink/1737583699704849/)

![image](../brief.jpg)

## Reason to start this project

It was Sep. 2019 when I read an articel about AutoBVM on Scientific American.AutoBVM is design for somewhere far away from hospital, patient from there have to squeeze 	BVM for days before they reach the site.So I try to make a cheap, eazy to build and battery powered BVM squeezer.

## Design of BVM squeezer

It is for patient transporting with someone beside and keep an eye on it.For an occasion like isolated medical site, it will need extra holding and monitoring machine.

The mechanism is used in bike breaking system. A nylon wire in a tube is pull by a turning rotor and drag an arm of the clamp, while the tube stay and hold the other arm of the clamp. 
So the clamp squeeze BVM and move air into the lung of patient.Then the rotor keep turning, wire and the arm return by auto-inflation of BVM.Flash air into the BVM and old one breath out by valve on BVM.

The clamp is a curve shape to minimized bending of BVM for longer life-time.

## BOM

1 Bag valve mask, BVM, or Ambu  
1 MDF sheet 600 * 450 mm    
1 JGY-370 worm-gear moter, 10 rpm at 12 V.  
1 Customized metal gear.    
1 Nylon wire.  
1 PE semi-rigid pipe.   
n M5 screw and nut  
n M3 screw and nut  